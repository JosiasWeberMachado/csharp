﻿
namespace trabalho_2
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dados = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.label1 = new System.Windows.Forms.Label();
            this.remover = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.altnome = new System.Windows.Forms.TextBox();
            this.alttelefone = new System.Windows.Forms.TextBox();
            this.altemail = new System.Windows.Forms.TextBox();
            this.altnumero = new System.Windows.Forms.TextBox();
            this.altcomplemento = new System.Windows.Forms.TextBox();
            this.altdata = new System.Windows.Forms.TextBox();
            this.altendereco = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dados
            // 
            this.dados.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.dados.FullRowSelect = true;
            this.dados.GridLines = true;
            this.dados.HideSelection = false;
            this.dados.Location = new System.Drawing.Point(12, 160);
            this.dados.MultiSelect = false;
            this.dados.Name = "dados";
            this.dados.Size = new System.Drawing.Size(853, 358);
            this.dados.TabIndex = 0;
            this.dados.UseCompatibleStateImageBehavior = false;
            this.dados.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Nome";
            this.columnHeader1.Width = 150;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Telefone";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "E-mail";
            this.columnHeader3.Width = 200;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Data Nascimento";
            this.columnHeader5.Width = 80;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Numero";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Complemento";
            this.columnHeader7.Width = 200;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(12, 130);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Usuarios Cadastrados:";
            // 
            // remover
            // 
            this.remover.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.remover.Location = new System.Drawing.Point(790, 130);
            this.remover.Name = "remover";
            this.remover.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.remover.Size = new System.Drawing.Size(75, 23);
            this.remover.TabIndex = 2;
            this.remover.Text = "Remover";
            this.remover.UseVisualStyleBackColor = false;
            this.remover.Click += new System.EventHandler(this.remover_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.Location = new System.Drawing.Point(709, 131);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Alterar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // altnome
            // 
            this.altnome.Location = new System.Drawing.Point(12, 33);
            this.altnome.Name = "altnome";
            this.altnome.Size = new System.Drawing.Size(123, 23);
            this.altnome.TabIndex = 4;
            // 
            // alttelefone
            // 
            this.alttelefone.Location = new System.Drawing.Point(150, 33);
            this.alttelefone.Name = "alttelefone";
            this.alttelefone.Size = new System.Drawing.Size(132, 23);
            this.alttelefone.TabIndex = 5;
            // 
            // altemail
            // 
            this.altemail.Location = new System.Drawing.Point(288, 33);
            this.altemail.Name = "altemail";
            this.altemail.Size = new System.Drawing.Size(132, 23);
            this.altemail.TabIndex = 6;
            // 
            // altnumero
            // 
            this.altnumero.Location = new System.Drawing.Point(150, 80);
            this.altnumero.Name = "altnumero";
            this.altnumero.Size = new System.Drawing.Size(132, 23);
            this.altnumero.TabIndex = 7;
            // 
            // altcomplemento
            // 
            this.altcomplemento.Location = new System.Drawing.Point(288, 80);
            this.altcomplemento.Name = "altcomplemento";
            this.altcomplemento.Size = new System.Drawing.Size(132, 23);
            this.altcomplemento.TabIndex = 8;
            // 
            // altdata
            // 
            this.altdata.Location = new System.Drawing.Point(426, 33);
            this.altdata.Name = "altdata";
            this.altdata.Size = new System.Drawing.Size(132, 23);
            this.altdata.TabIndex = 9;
            // 
            // altendereco
            // 
            this.altendereco.Location = new System.Drawing.Point(12, 80);
            this.altendereco.Name = "altendereco";
            this.altendereco.Size = new System.Drawing.Size(123, 23);
            this.altendereco.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(150, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 15);
            this.label3.TabIndex = 12;
            this.label3.Text = "Telefone";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(288, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 15);
            this.label4.TabIndex = 13;
            this.label4.Text = "E-mail";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(426, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 15);
            this.label5.TabIndex = 14;
            this.label5.Text = "Data de Nascimento";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 15);
            this.label6.TabIndex = 15;
            this.label6.Text = "Endereço";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(150, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 15);
            this.label7.TabIndex = 16;
            this.label7.Text = "Numero";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(288, 62);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 15);
            this.label8.TabIndex = 17;
            this.label8.Text = "Complemento";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 15);
            this.label2.TabIndex = 18;
            this.label2.Text = "Nome";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 530);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.altendereco);
            this.Controls.Add(this.altdata);
            this.Controls.Add(this.altcomplemento);
            this.Controls.Add(this.altnumero);
            this.Controls.Add(this.altemail);
            this.Controls.Add(this.alttelefone);
            this.Controls.Add(this.altnome);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.remover);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dados);
            this.Name = "Form2";
            this.Text = "Cadastros";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView dados;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.Button remover;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox altnome;
        private System.Windows.Forms.TextBox alttelefone;
        private System.Windows.Forms.TextBox altemail;
        private System.Windows.Forms.TextBox altnumero;
        private System.Windows.Forms.TextBox altcomplemento;
        private System.Windows.Forms.TextBox altdata;
        private System.Windows.Forms.TextBox altendereco;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
    }
}