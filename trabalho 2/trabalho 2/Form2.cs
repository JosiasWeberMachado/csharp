﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace trabalho_2
{
    public partial class Form2 : Form
    {
        Form1 call;

        public Form2(Form1 chamador)
        {
            call = chamador;
            InitializeComponent();
        }

        private void remover_Click(object sender, EventArgs e)
        {
            if (dados.SelectedIndices.Count > 0)
            {
                dados.Items.RemoveAt(dados.SelectedIndices[0]);
            }
        }

        public ListView DADOS { 

            get { return this.dados; }
            set { this.dados = value; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (altnome.Text.Length != 0 && alttelefone.Text.Length != 0 && altemail.Text.Length != 0 && altdata.Text.Length != 0 && altendereco.Text.Length != 0
            && altnumero.Text.Length != 0 && altcomplemento.Text.Length != 0)
            {
                string[] item = new string[7];
                item[0] = altnome.Text;
                item[1] = alttelefone.Text;
                item[2] = altemail.Text;
                item[3] = altdata.Text;
                item[4] = altendereco.Text;
                item[5] = altnumero.Text;
                item[6] = altcomplemento.Text;

                ListViewItem altera = new ListViewItem(item);
                dados.Items[dados.SelectedIndices[0]] = altera;
            }


        }   
    }
}
