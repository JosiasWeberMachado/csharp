﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace trabalho_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void cad_Click(object sender, EventArgs e)
        {
            Form2 cadastros = new Form2(this);
            cadastros.Show();

            string[] dadosclientes = new string[7];
            dadosclientes[0] = nome.Text;
            dadosclientes[1] = telefone.Text;
            dadosclientes[2] = email.Text;
            dadosclientes[3] = data.Text;
            dadosclientes[4] = endereco.Text;
            dadosclientes[5] = numero.Text;
            dadosclientes[6] = complemento.Text;

            ListViewItem clientes = new ListViewItem(dadosclientes);

            cadastros.DADOS.Items.Add(clientes);
        }
            
    }

}
