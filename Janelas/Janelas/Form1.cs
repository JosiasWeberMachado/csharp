﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Janelas
{
    public partial class Form1 : Form
    {
        int qtdJanelas;
        public Form1()
        {
            
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 novo = new Form2(qtdJanelas, this);
            novo.Show();
        }

        public string Label1 {
            get { return label1.Text; }
            set {label1.Text = value;}
        }
    }
}
