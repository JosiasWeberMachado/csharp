﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Janelas
{
    public partial class Form2 : Form
    {
        int ordem;
        Form1 origem;
        public Form2(int pos, Form1 chamador)
        {
            ordem = pos;
            origem = chamador;
            InitializeComponent();
            label1.Text = "janela numero:" + ordem;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            origem.Label1 = textBox1.Text;
        }
    }
}
