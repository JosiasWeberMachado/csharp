﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace listbox
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text != "") { 
            listBox1.Items.Add(textBox1.Text + "-" + comboBox1.SelectedItem.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItems.Count > 0) { 
            listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            }
            else
            {
                MessageBox.Show("Selecione um elemento","Atenção!");
            }
        }

        private void busca_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listBox1.Items.Count; i++) 
            {
                if (listBox1.Items[i].ToString().IndexOf(textBox2.Text) != -1)
                {
                    listBox1.SelectedIndex = i;
                }
            }
        }
    }
}
