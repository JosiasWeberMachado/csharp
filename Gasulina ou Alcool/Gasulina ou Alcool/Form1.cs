﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gasulina_ou_Alcool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void btn_Click(object sender, EventArgs e)
        {
            double  varalcool = double.Parse(txtA.Text);
            double  vargasolina = double.Parse(txtG.Text);
            double  proporcao = varalcool / vargasolina;

            if(proporcao < 0.7)
            {
                lblresultado.Text = proporcao.ToString("N2") + " - Use Alcool";
            }
            else
            {
                lblresultado.Text = proporcao.ToString("N2") + " - Use Gasolina";
            }
        }
    }
}
