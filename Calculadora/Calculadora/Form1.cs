﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        double operando1, operando2;
        string operacao;
        bool operandoTrocado = false;

        public Form1()
        {
            operando1 = 0;
            operando2 = 0;
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void divide_Click(object sender, EventArgs e)
        {
            operandoTrocado = true;
            operando1 = double.Parse(textNumeros.Text);
            textNumeros.Text = "";
            operacao = "/";
        }

        private void mutiplica_Click(object sender, EventArgs e)
        {
            operando1 = double.Parse(textNumeros.Text);
            textNumeros.Text = "";
            operacao = "*";
        }

        private void subtrai_Click(object sender, EventArgs e)
        {
            operando1 = double.Parse(textNumeros.Text);
            textNumeros.Text = "";
            operacao = "-";
        }

        private void adiciona_Click(object sender, EventArgs e)
        {
            operando1 = double.Parse(textNumeros.Text);
            textNumeros.Text = "";
            operacao = "+";
        }

        private void CE_Click(object sender, EventArgs e)
        {
            textNumeros.Text = "";
        }

        private void igual_Click(object sender, EventArgs e)
        {
            operando2 = double.Parse(textNumeros.Text);
            textNumeros.Text = "";

            if (operacao == "/")
            {
                textNumeros.Text = (operando1 / operando2).ToString();
            }

            if (operacao == "*")
            {
                textNumeros.Text = (operando1 * operando2).ToString();
            }

            if (operacao == "-")
            {
                textNumeros.Text = (operando1 - operando2).ToString();
            }

            if (operacao == "+")
            {
                textNumeros.Text = (operando1 + operando2).ToString();
            }
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            textNumeros.Text += btn7.Text;
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            textNumeros.Text += btn8.Text;
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            textNumeros.Text += btn9.Text;
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            textNumeros.Text += btn4.Text;
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            textNumeros.Text += btn5.Text;
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            textNumeros.Text += btn6.Text;
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            textNumeros.Text += btn1.Text;
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            textNumeros.Text += btn2.Text;
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            textNumeros.Text += btn3.Text;
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            textNumeros.Text += btn0.Text;
        }

        

    }
}
