﻿
namespace Calculadora
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textNumeros = new System.Windows.Forms.TextBox();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.igual = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.adiciona = new System.Windows.Forms.Button();
            this.subtrai = new System.Windows.Forms.Button();
            this.mutiplica = new System.Windows.Forms.Button();
            this.divide = new System.Windows.Forms.Button();
            this.CE = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textNumeros
            // 
            this.textNumeros.Location = new System.Drawing.Point(13, 13);
            this.textNumeros.Name = "textNumeros";
            this.textNumeros.Size = new System.Drawing.Size(267, 23);
            this.textNumeros.TabIndex = 0;
            // 
            // btn7
            // 
            this.btn7.Location = new System.Drawing.Point(12, 107);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(56, 47);
            this.btn7.TabIndex = 1;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn8
            // 
            this.btn8.Location = new System.Drawing.Point(74, 107);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(56, 47);
            this.btn8.TabIndex = 2;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn9
            // 
            this.btn9.Location = new System.Drawing.Point(136, 107);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(56, 47);
            this.btn9.TabIndex = 3;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btn6
            // 
            this.btn6.Location = new System.Drawing.Point(136, 160);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(56, 47);
            this.btn6.TabIndex = 7;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn5
            // 
            this.btn5.Location = new System.Drawing.Point(74, 160);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(56, 47);
            this.btn5.TabIndex = 6;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn4
            // 
            this.btn4.Location = new System.Drawing.Point(12, 160);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(56, 47);
            this.btn4.TabIndex = 5;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn3
            // 
            this.btn3.Location = new System.Drawing.Point(136, 213);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(56, 47);
            this.btn3.TabIndex = 10;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(74, 213);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(56, 47);
            this.btn2.TabIndex = 9;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(12, 213);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(56, 47);
            this.btn1.TabIndex = 8;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // igual
            // 
            this.igual.Location = new System.Drawing.Point(136, 266);
            this.igual.Name = "igual";
            this.igual.Size = new System.Drawing.Size(56, 47);
            this.igual.TabIndex = 12;
            this.igual.Text = "=";
            this.igual.UseVisualStyleBackColor = true;
            this.igual.Click += new System.EventHandler(this.igual_Click);
            // 
            // btn0
            // 
            this.btn0.Location = new System.Drawing.Point(12, 266);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(118, 47);
            this.btn0.TabIndex = 11;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // adiciona
            // 
            this.adiciona.Location = new System.Drawing.Point(224, 266);
            this.adiciona.Name = "adiciona";
            this.adiciona.Size = new System.Drawing.Size(56, 47);
            this.adiciona.TabIndex = 16;
            this.adiciona.Text = "+";
            this.adiciona.UseVisualStyleBackColor = true;
            this.adiciona.Click += new System.EventHandler(this.adiciona_Click);
            // 
            // subtrai
            // 
            this.subtrai.Location = new System.Drawing.Point(224, 213);
            this.subtrai.Name = "subtrai";
            this.subtrai.Size = new System.Drawing.Size(56, 47);
            this.subtrai.TabIndex = 15;
            this.subtrai.Text = "-";
            this.subtrai.UseVisualStyleBackColor = true;
            this.subtrai.Click += new System.EventHandler(this.subtrai_Click);
            // 
            // mutiplica
            // 
            this.mutiplica.Location = new System.Drawing.Point(224, 160);
            this.mutiplica.Name = "mutiplica";
            this.mutiplica.Size = new System.Drawing.Size(56, 47);
            this.mutiplica.TabIndex = 14;
            this.mutiplica.Text = "*";
            this.mutiplica.UseVisualStyleBackColor = true;
            this.mutiplica.Click += new System.EventHandler(this.mutiplica_Click);
            // 
            // divide
            // 
            this.divide.Location = new System.Drawing.Point(224, 107);
            this.divide.Name = "divide";
            this.divide.Size = new System.Drawing.Size(56, 47);
            this.divide.TabIndex = 13;
            this.divide.Text = "/";
            this.divide.UseVisualStyleBackColor = true;
            this.divide.Click += new System.EventHandler(this.divide_Click);
            // 
            // CE
            // 
            this.CE.Location = new System.Drawing.Point(12, 54);
            this.CE.Name = "CE";
            this.CE.Size = new System.Drawing.Size(56, 47);
            this.CE.TabIndex = 17;
            this.CE.Text = "CE";
            this.CE.UseVisualStyleBackColor = true;
            this.CE.Click += new System.EventHandler(this.CE_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 323);
            this.Controls.Add(this.CE);
            this.Controls.Add(this.adiciona);
            this.Controls.Add(this.subtrai);
            this.Controls.Add(this.mutiplica);
            this.Controls.Add(this.divide);
            this.Controls.Add(this.igual);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.textNumeros);
            this.Name = "Form1";
            this.Text = " ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textNumeros;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button igual;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button adiciona;
        private System.Windows.Forms.Button subtrai;
        private System.Windows.Forms.Button mutiplica;
        private System.Windows.Forms.Button divide;
        private System.Windows.Forms.Button CE;
    }
}

