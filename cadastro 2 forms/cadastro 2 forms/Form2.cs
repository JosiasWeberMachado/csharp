﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cadastro_2_forms
{
    public partial class Form2 : Form
    {
        Form1 call;

        public Form2(Form1 lista)
        {
            call = lista;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
                call.Label1 = nome.Text + " " + sobrenome.Text;
        }

        private void button1_KeyPress(object sender, KeyPressEventArgs e)
        {
                call.Label1 = nome.Text + " " + sobrenome.Text;
        }
    }
}
