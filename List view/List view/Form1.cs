﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace List_view
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            string[] p = new string[2];
            p[0] = "Josias";
            p[1] = "24";

            ListViewItem item1 = new ListViewItem(p);

            listView1.Items.Add(item1);
            listView1.Items.Add(new ListViewItem(new string[] {"maria", "18" }));
            listView1.Items.Add(new ListViewItem(new string[] { "SLA", "20" }));
            listView1.Items.Add(new ListViewItem(new string[] { "Algo", "30" }));

            for (int i = 0; i < listView1.Items.Count; i++)
            {
                ListViewItem temp = listView1.Items[i];
                label1.Text = label1.Text + temp.SubItems[0].Text + " " + temp.SubItems[1].Text + "\n";
            }

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(listView1.SelectedIndices.Count > 0)
            {
                listView1.Items.RemoveAt(listView1.SelectedIndices[0]);

                label1.Text = "";
                for (int i = 0; i < listView1.Items.Count; i++)
                {
                    ListViewItem temp = listView1.Items[i];
                    label1.Text = label1.Text + temp.SubItems[0].Text + " " + temp.SubItems[1].Text + "\n";
                }

            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(nome.Text.Length != 0 && idade.Text.Length != 0)
            {
                string[] item = new string[2];
                item[0] = nome.Text;
                item[1] = idade.Text;

                ListViewItem altera = new ListViewItem(item);
                listView1.Items[listView1.SelectedIndices[0]] = altera;
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string[] item = new string[2];
            item[0] = nome.Text;
            item[1] = idade.Text;

            ListViewItem lvitem = new ListViewItem(item);

            listView1.Items.Add(lvitem);
        }
    }
}
